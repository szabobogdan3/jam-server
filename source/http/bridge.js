
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

class HttpBridge {

  constructor(storage) {
    this.storage = storage;
  }

  get requestPayloadFunction() {
    const singularName = this.storage.singular;

    return function(request) {
      if(!request.payload || !request.payload[singularName]) {
        return Boom.badRequest(`You must send an object containing a "${singularName}" object.`);
      }

      if(Object.keys(request.payload[singularName]).length == 0) {
        return Boom.badRequest(`You must send at least one field for the "${singularName}" object.`);
      }

      return request.payload[singularName];
    }
  }

  get schemaValidationFunction() {
    let schema = Joi.any();

    if(this.storage.schema) {
      schema = this.storage.schema.required();
    }

    return function(invalidatedValue) {
      const { error, value } = schema.validate(invalidatedValue);

      if(error) {
        return Boom.badRequest(error);
      }

      return value;
    }
  }

  bindCreate(server) {
    const storage = this.storage;
    const singularName = storage.singular;
    const validateSchema = this.schemaValidationFunction;
    const requestPayload = this.requestPayloadFunction;

    server.route({
      method: 'POST',
      path: `/${this.storage.plural}`,
      async handler(request) {
        const payload = requestPayload(request);

        if(payload.isBoom) {
          return payload;
        }

        let value = validateSchema(payload);

        if(value.isBoom) {
          return value;
        }

        const result = {};
        result[singularName] = storage.create(value);

        return result;
      }
    });
  }

  bindUpdate(server) {
    const storage = this.storage;
    const singularName = storage.singular;
    const validateSchema = this.schemaValidationFunction;
    const requestPayload = this.requestPayloadFunction;

    server.route({
      method: 'PATCH',
      path: `/${this.storage.plural}/{id}`,
      async handler(request) {
        const _id = request.params.id;
        const storedValue = storage.getItem(_id);

        if(!storage.exists(_id)) {
          return Boom.notFound(`There is no '${singularName}' with the id '${request.params.id}'.`)
        }

        const payload = requestPayload(request);

        if(payload.isBoom) {
          return payload;
        }

        if(payload.hasOwnProperty("_id") && payload._id != storedValue._id) {
          return Boom.badRequest(`The '${singularName}' has an invalid id.`);
        }

        const newRecord = validateSchema(Object.assign({}, storedValue, payload, { _id: storedValue._id }));

        if(newRecord.isBoom) {
          return newRecord;
        }

        storage.update(newRecord)

        const result = {};
        result[singularName] = newRecord;

        return result;
      }
    });
  }

  bindQuery(server) {
    const storage = this.storage;
    const pluralName = storage.plural;

    server.route({
      method: 'GET',
      path: `/${this.storage.plural}`,
      async handler(request) {
        const result = {};

        if(request.query && Object.keys(request.query).length > 0) {
          result[pluralName] = storage.query(request.query);
        } else {
          result[pluralName] = storage.query();
        }

        return result;
      }
    });
  }

  bind(server) {
    if(this.storage.create) {
      this.bindCreate(server);
    }

    if(this.storage.getItem && this.storage.update) {
      this.bindUpdate(server);
    }

    if(this.storage.query) {
      this.bindQuery(server);
    }
  }
}

module.exports = HttpBridge;