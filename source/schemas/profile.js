const Joi = require('@hapi/joi');

const schema = Joi.object({
  _id: [
    Joi.string(),
    Joi.number()
  ],

  name: Joi.string()
    .min(3)
    .max(60)
    .required(),

  email: Joi.string()
    .email({ minDomainSegments: 2 }),

  instrument: Joi.string()
    .alphanum()
    .min(3)
    .max(60)
});

module.exports = schema;