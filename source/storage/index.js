const ProfileStorage = require('./profile');
const SongStorage = require('./song');
const JamStorage = require('./jam');

module.exports = {
  create() {
    return [ new ProfileStorage(), new SongStorage(), new JamStorage() ];
  }
}