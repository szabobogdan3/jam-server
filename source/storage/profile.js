const ProfileSchema = require('../schemas/profile');
const MemoryStorage = require('./memory');


class ProfileStorage extends MemoryStorage {
  singular = 'profile'
  plural = 'profiles'
  items = []

  constructor() {
    super(...arguments);

    this.schema = ProfileSchema;
  }
}


module.exports = ProfileStorage;