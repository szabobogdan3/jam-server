const MemoryStorage = require('./memory');

class SongStorage extends MemoryStorage {
  singular = 'song'
  plural = 'songs'
}

module.exports = SongStorage;