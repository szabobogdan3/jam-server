const MemoryStorage = require('./memory');

class JamStorage extends MemoryStorage {
  singular = 'jam'
  plural = 'jams'
}

module.exports = JamStorage;