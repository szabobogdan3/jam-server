
function clone(value) {
  return JSON.parse(JSON.stringify(value));
}

class MemoryStorage {
  items = []

  create(value) {
    const _id = this.items.length;
    const newRecord = clone(value);
    newRecord._id = _id;

    this.items.push(newRecord);

    return this.getItem(_id);
  }

  update(value) {
    const _id = parseInt(value._id);
    const record = clone(value);
    this.items[_id] = record;

    return this.getItem(_id);
  }

  getItem(_id) {
    const intId = parseInt(_id);

    return clone(this.items[intId]);
  }

  query(params) {
    if(!params || Object.keys(params).length == 0) {
      return clone(this.items);
    }

    function isMatch(value) {
      let matched = true;

      Object.keys(params).forEach(key => {
        if(params[key] != value[key]) {
          matched = false;
        }
      });

      return matched;
    }

    return clone(this.items.filter(isMatch));
  }

  exists(_id) {
    const intId = parseInt(_id);

    return !!this.items[intId];
  }

  get size() {
    return this.items.length;
  }
}


module.exports = MemoryStorage;