const Hapi = require('@hapi/hapi');
const HttpBridge = require('./http/bridge');

exports.init = async function(storageObjects, log) {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  });

  const bridges = storageObjects.map(a => new HttpBridge(a));

  bridges.forEach(bridge => {
    bridge.bind(server);
  });

  if(log) {
    console.log('Server running on %s', server.info.uri);
  }

  return server;
};
