const Server = require('./source/server');

const Storage = require('./source/storage')

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

const main = async () => {
  const server = await Server.init(Storage.create(), true);

  await server.start();
};

main();