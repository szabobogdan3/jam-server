FROM node:14-alpine

LABEL mantainer="Szabo Bogdan <contact@szabobogdan.com>"

# Copy the app files
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node source source
COPY --chown=node:node index.js index.js

# The container entrypoint
CMD [ "node", "index.js" ]
