# Jam Server

This is a node js server which gives you access to models that allows you to 
organise music jams.

## Running

```
git clone https://gitlab.com/szabobogdan3/jam-server.git

cd jam-server

npm install

node index.js

```

or you can use one of the docker images:

[https://gitlab.com/szabobogdan3/jam-server/container_registry](https://gitlab.com/szabobogdan3/jam-server/container_registry)


## Running tests

The tests are written using mocha and chai. You can un them using the following 
command:

```
npm tests
```


## Models

The supported models are: `jam`, `song` and `profile`. The storage is binded to 
the http server by the `HttpBridge` class and it exposes the following routes:

- `GET /{model}?[query params]` to list the stored records
- `POST /{model}` to create new records
- `PATCH /{model}/{id}` to update fields in the existing records

The server stores the records in memory and all the data is lost after a restart. 
Persistance can be added by flushing the stored data to a file, or extending the 
storage classes to use a database storage, like mongo.