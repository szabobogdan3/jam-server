require('chai').should();
const { init, destroy } = require('./setup');

describe('GET /', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  it('responds with 404', async function () {
    const response = await server.inject({
      method: 'get',
      url: '/'
    });

    response.statusCode.should.equal(404);
  });
});