require('chai').should();
const { init, destroy, getStorage } = require('../setup');
const { jamPending, jamLive } = require('../../testData/jams');

describe('GET /jams', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  describe("when there are no stored jams", function() {
    it('responds with 200', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/jams'
      });

      response.statusCode.should.equal(200);
    });

    it('responds with an empty jams list', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/jams'
      });

      response.result.should.deep.equal({ jams: [ ] });
    });
  });

  describe("when there are two stored jams one pending and one live", function() {
    beforeEach(function() {
      getStorage("jams").create(jamPending);
      getStorage("jams").create(jamLive);
    });

    it('responds with a list containing the jams', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/jams'
      });

      const jam1 = Object.assign({}, jamPending, { _id: 0 });
      const jam2 = Object.assign({}, jamLive, { _id: 1 });

      response.result.should.deep.equal({ jams: [ jam1, jam2 ] });
    });

    it('responds with a list containing the pending jam when the "status" query param is "pending"', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/jams?status=pending'
      });

      const jam = Object.assign({}, jamPending, { _id: 0 });

      response.result.should.deep.equal({ jams: [ jam ] });
    });

    it('responds with a list containing the pending jam when the "status" query param is "pending"', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/jams?status=live'
      });

      const jam = Object.assign({}, jamLive, { _id: 1 });

      response.result.should.deep.equal({ jams: [ jam ] });
    });
  });
});
