require('chai').should();
const { init, destroy } = require('../setup');
const { profileWithEmailNameAndInstrument, profileWithEmailAndName } = require('../../testData/profiles');

describe('POST /profiles', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  describe("with a profile with email, name and instrument", function() {
    it('responds with 200', async function () {
      const response = await server.inject({
        method: 'post',
        url: '/profiles',
        payload: {
          profile: profileWithEmailNameAndInstrument
        }
      });

      response.statusCode.should.equal(200);
    });

    it('responds with the created record', async function () {
      const response = await server.inject({
        method: 'post',
        url: '/profiles',
        payload: {
          profile: profileWithEmailNameAndInstrument
        }
      });

      const expectedProfile = Object.assign(profileWithEmailNameAndInstrument, { _id: 0 });

      response.result.should.deep.equal({ profile: expectedProfile });
    });
  });

  describe("with a profile with email and name", function() {
    it('responds with 200', async function () {
      const response = await server.inject({
        method: 'post',
        url: '/profiles',
        payload: {
          profile: profileWithEmailNameAndInstrument
        }
      });

      response.statusCode.should.equal(200);
    });

    it('responds with the created record', async function () {
      const response = await server.inject({
        method: 'post',
        url: '/profiles',
        payload: {
          profile: profileWithEmailAndName
        }
      });

      const expectedProfile = Object.assign(profileWithEmailAndName, { _id: 0 });

      response.result.should.deep.equal({ profile: expectedProfile });
    });
  });

  describe("with a profile with no fields", function() {
    it('responds with 400', async function () {
      const response = await server.inject({
        method: 'post',
        url: '/profiles',
        payload: {
          profile: { }
        }
      });

      response.statusCode.should.equal(400);
    });
  });
});
