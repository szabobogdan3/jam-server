require('chai').should();
const { init, destroy, getStorage } = require('../setup');
const { profileWithEmailNameAndInstrument, profileWithEmailAndName } = require('../../testData/profiles');

describe('PATCH /profiles/{id}', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  describe('when a profile with name, email and _id = 0 exists', function() {
    beforeEach(function() {
      getStorage("profiles").create(profileWithEmailAndName);
    });

    it('responds with 200', async function () {
      const response = await server.inject({
        method: 'patch',
        url: '/profiles/0',
        payload: {
          profile: {
            instrument: "base"
          }
        }
      });

      response.statusCode.should.equal(200);
    });

    it('responds with the updated object', async function () {
      const response = await server.inject({
        method: 'patch',
        url: '/profiles/0',
        payload: {
          profile: {
            instrument: "base"
          }
        }
      });

      const expectedProfile = Object.assign({}, profileWithEmailAndName, { _id: 0, instrument: "base" });
      response.result.should.deep.equal({
        profile: expectedProfile
      });
    });

    describe("sending an undefined property", function() {
      it('responds with 400', async function () {
        const response = await server.inject({
          method: 'patch',
          url: '/profiles/0',
          payload: {
            profile: {
              something: "not random"
            }
          }
        });

        response.statusCode.should.equal(400);
        response.result.should.deep.equal({
          "error": "Bad Request",
          "message": `"something" is not allowed`,
          "statusCode": 400
        });
      });
    });
  });
});
