require('chai').should();

const HttpBridge = require('../../../source/http/bridge');
const Hapi = require('@hapi/hapi');
const { expect } = require('chai');

describe('The HttpBridge query', function () {
  let server;

  beforeEach(async function () {
    server = Hapi.server({
      port: 3000,
      host: 'localhost'
    });
  });

  afterEach(async function () {
    await server.stop();
  });

  describe("for an object with the `query` method", function () {
    let objectWithQuery;
    let bridge;
    let items;
    let query;

    beforeEach(function () {
      items = [];

      objectWithQuery = {
        plural: "manyItems",
        singular: "oneItem",

        query(params) {
          query = params;
          return items;
        }
      }

      query = null;
      bridge = new HttpBridge(objectWithQuery);
    });

    it('should add the `GET /manyItems` route', function () {
      bridge.bind(server);

      const serverRoutes = server.table();

      expect(serverRoutes[0].method).to.equal("get");
      expect(serverRoutes[0].path).to.equal("/manyItems");
    });

    describe("when there are no stored items", function() {
      it('returns an empty list', async function () {
        bridge.bind(server);

        const response = await server.inject({
          method: 'get',
          url: '/manyItems'
        });

        response.statusCode.should.equal(200);
        response.result.should.deep.equal({
          manyItems: []
        });
      });

      it('should not send the query params to the storage if they are not set in the url', async function () {
        bridge.bind(server);

        await server.inject({
          method: 'get',
          url: '/manyItems'
        });

        expect(query).to.equal(undefined);
      });

      it('should send the query params to the storage if they are set in the url', async function () {
        bridge.bind(server);

        await server.inject({
          method: 'get',
          url: '/manyItems?param1=value1&param2=value2'
        });

        expect(query).to.deep.equal({
          param1: "value1",
          param2: "value2"
        });
      });
    });

    describe("when there is a stored item", function() {
      beforeEach(function() {
        items = [ { _id: 0 } ];
      });

      it("should return the item list", async function() {
        bridge.bind(server);

        const response = await server.inject({
          method: 'get',
          url: '/manyItems'
        });

        response.statusCode.should.equal(200);
        response.result.should.deep.equal({
          manyItems: [ { _id: 0 } ]
        });
      });
    });
  });
});
