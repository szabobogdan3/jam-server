require('chai').should();

const HttpBridge = require('../../../source/http/bridge');
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const { expect } = require('chai');

describe('The HttpBridge update', function () {
  let server;

  beforeEach(async function () {
    server = Hapi.server({
      port: 3000,
      host: 'localhost'
    });
  });

  afterEach(async function () {
    await server.stop();
  });

  describe("for an object with the `getItem` and `update` methods", function () {
    let objectWithCreate;
    let bridge;
    let updatedValue;

    beforeEach(function () {
      objectWithCreate = {
        plural: "plural",
        singular: "singular",

        exists(_id) {
          return parseInt(_id) < 5;
        },

        getItem(_id) {
          return { _id: parseInt(_id), name: "John" }
        },

        update(value) {
          updatedValue = value;
          return value || "missing value";
        }
      }

      updatedValue = null;
      bridge = new HttpBridge(objectWithCreate);
    });

    it('should add the `PATCH /plural/{id}` route', function () {
      bridge.bind(server);

      const serverRoutes = server.table();

      expect(serverRoutes[0].method).to.equal("patch");
      expect(serverRoutes[0].path).to.equal("/plural/{id}");
    });

    it('returns the stored value with the added new field when `PATCH /plural/0` is called', async function () {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0',
        payload: {
          singular: {
            other: "value"
          }
        }
      });

      response.statusCode.should.equal(200);
      response.result.should.deep.equal({
        singular: {
          _id: 0,
          name: "John",
          other: "value"
        }
      });
    });

    it('return a 400 error when the value id does not match the url id', async function () {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0',
        payload: {
          singular: {
            _id: 2
          }
        }
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": "The 'singular' has an invalid id.",
        "statusCode": 400
      });
    });

    it('return a 404 error when there is no record with the provided id', async function () {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/10',
        payload: {
          singular: {}
        }
      });

      response.statusCode.should.equal(404);
      response.result.should.deep.equal({
        "error": "Not Found",
        "message": "There is no 'singular' with the id '10'.",
        "statusCode": 404
      });
    });

    it("should return 400 when nothing is sent", async function() {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0'
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": `You must send an object containing a "singular" object.`,
        "statusCode": 400
      });
    });

    it("should return 400 when an empty payload is sent", async function() {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0',
        payload: {
        }
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": `You must send an object containing a "singular" object.`,
        "statusCode": 400
      });
    });

    it("should return 400 when a payload with an empty object is sent", async function() {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0',
        payload: {
          singular: {}
        }
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": `You must send at least one field for the "singular" object.`,
        "statusCode": 400
      });
    });

    it("should return 400 when a payload with a number sent", async function() {
      bridge.bind(server);

      const response = await server.inject({
        method: 'patch',
        url: '/plural/0',
        payload: 3
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": `You must send an object containing a "singular" object.`,
        "statusCode": 400
      });
    });

    describe("when a schema is defined", function() {
      beforeEach(function() {
        objectWithCreate.schema = Joi.object({
          _id: Joi.any(),
          name: Joi.any(),
          age: Joi.number().positive()
        });
      });

      it("should return 400 when an undefined field is sent", async function() {
        bridge.bind(server);

        const response = await server.inject({
          method: 'patch',
          url: '/plural/0',
          payload: {
            singular: {
              coolness: 'high'
            }
          }
        });

        response.statusCode.should.equal(400);
        response.result.should.deep.equal({
          "error": "Bad Request",
          "message": `"coolness" is not allowed`,
          "statusCode": 400
        });
      });

      it("should not update the stored value when an undefined field is sent", async function() {
        bridge.bind(server);

        await server.inject({
          method: 'patch',
          url: '/plural/0',
          payload: {
            singular: {
              coolness: 'high'
            }
          }
        });

        expect(updatedValue).to.equal(null);
      });
    });
  });
});
