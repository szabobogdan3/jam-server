require('chai').should();

const HttpBridge = require('../../../source/http/bridge');
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const { expect } = require('chai');

describe('The HttpBridge create', function () {
  let server;

  beforeEach(async function () {
    server = Hapi.server({
      port: 3000,
      host: 'localhost'
    });
  });

  afterEach(async function () {
    await server.stop();
  });

  describe("for an object with the `create` method", function () {
    let objectWithCreate;
    let bridge;
    let createdValue;

    beforeEach(function () {
      objectWithCreate = {
        plural: "plural",
        singular: "singular",

        create(value) {
          createdValue = value;
          return value || "the result";
        }
      }

      createdValue = null;
      bridge = new HttpBridge(objectWithCreate);
    });

    it('should add the `POST /plural` route', function () {
      bridge.bind(server);

      const serverRoutes = server.table();

      expect(serverRoutes.length).to.equal(1);
      expect(serverRoutes[0].method).to.equal("post");
      expect(serverRoutes[0].path).to.equal("/plural");
    });

    it('returns the function result when `POST /plural` is called', async function () {
      bridge.bind(server);

      const response = await server.inject({
        method: 'post',
        url: '/plural',
        payload: {
          singular: "some message"
        }
      });

      response.statusCode.should.equal(200);
      response.result.should.deep.equal({
        singular: "some message"
      });
    });

    it('sends the singular property of the `POST /plural` request body as parameter', async function () {
      bridge.bind(server);

      const response = await server.inject({
        method: 'post',
        url: '/plural',
        payload: {
          singular: "some value"
        }
      });

      response.result.should.deep.equal({ singular: "some value" });
      response.statusCode.should.equal(200);
    });

    it("should return 400 when nothing is sent", async function() {
      bridge.bind(server);

      const response = await server.inject({
        method: 'post',
        url: '/plural'
      });

      response.statusCode.should.equal(400);
      response.result.should.deep.equal({
        "error": "Bad Request",
        "message": `You must send an object containing a "singular" object.`,
        "statusCode": 400
      });
    });

    describe("when a schema is defined", function() {
      beforeEach(function() {
        objectWithCreate.schema = Joi.object({
          _id: Joi.any(),
          name: Joi.any(),
          age: Joi.number().positive()
        });
      });

      it("should return 400 when an undefined field is sent", async function() {
        bridge.bind(server);

        const response = await server.inject({
          method: 'post',
          url: '/plural',
          payload: {
            singular: {
              coolness: 'high'
            }
          }
        });

        response.statusCode.should.equal(400);
        response.result.should.deep.equal({
          "error": "Bad Request",
          "message": `"coolness" is not allowed`,
          "statusCode": 400
        });
      });

      it("should not add the value when an undefined field is sent", async function() {
        bridge.bind(server);

        await server.inject({
          method: 'patch',
          url: '/plural/0',
          payload: {
            singular: {
              coolness: 'high'
            }
          }
        });

        expect(createdValue).to.equal(null);
      });
    });
  });
});
