const Server = require('../../source/server');
const Storage = require('../../source/storage')

let server;
let storages;

module.exports.getStorage = function(name) {
  return storages.filter(a => a.plural == name)[0];
}

module.exports.init = async function() {
  storages = Storage.create();

  server = await Server.init(storages);

  return server;
}

module.exports.destroy = async function() {
  if(server) {
    await server.stop();
  }
}

process.on('unhandledRejection', (err) => {
  module.exports.destroy();
});