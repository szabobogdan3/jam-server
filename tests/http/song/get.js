require('chai').should();
const { init, destroy, getStorage } = require('../setup');
const { songWithAllFields } = require('../../testData/songs');

describe('GET /songs', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  describe("when there are no stored songs", function() {
    it('responds with 200', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/songs'
      });

      response.statusCode.should.equal(200);
    });

    it('responds with an empty song list', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/songs'
      });

      response.result.should.deep.equal({ songs: [ ] });
    });
  });

  describe("when there is a stored song", function() {
    beforeEach(function() {
      getStorage("songs").create(songWithAllFields);
    });

    it('responds with an song list containing the record', async function () {
      const response = await server.inject({
        method: 'get',
        url: '/songs'
      });

      const song = Object.assign({}, songWithAllFields, { _id: 0 });

      response.result.should.deep.equal({ songs: [ song ] });
    });
  });
});
