require('chai').should();

const ProfileStorage = require('../../source/storage/profile')
const { profileWithEmailNameAndInstrument } = require('../testData/profiles');

describe('A ProfileStorage instance', function () {
  let storage;

  beforeEach(function() {
    storage = new ProfileStorage();
  });

  describe("when it is empty", function() {
    it('should have the size 0', function() {
      storage.size.should.deep.equal(0);
    });

    it('should return false when checking if a record exists', function() {
      storage.exists(0).should.equal(false);
    });
  });

  describe("creating records", function() {
    it('assigns an _id and returns the stored value', async function () {
      storage.create(profileWithEmailNameAndInstrument)
        .should.deep.equal({
          "_id": 0,
          "email": "john@me.com",
          "instrument": "guitar",
          "name": "John Doe"
        });
    });

    it('should have the size 1', function() {
      storage.create(profileWithEmailNameAndInstrument);
      storage.size.should.equal(1);
    });

    it('should exist', function() {
      storage.create(profileWithEmailNameAndInstrument);
      storage.exists(0).should.equal(true);
    });
  })

  describe("updating an existing record", function() {
    it('returns the stored value', async function () {
      const item = storage.create(profileWithEmailNameAndInstrument);
      item["instrument"] = "base";

      const updatedItem = storage.update(item);
      updatedItem.should.deep.equal({
          "_id": 0,
          "email": "john@me.com",
          "instrument": "base",
          "name": "John Doe"
        });
    });

    it('should have the size 1', function() {
      const item = storage.create(profileWithEmailNameAndInstrument);
      item["instrument"] = "base";
      storage.update(item);

      storage.size.should.equal(1);
    });

    it('should be retrieved using the id', function() {
      const item = storage.create(profileWithEmailNameAndInstrument);
      item["instrument"] = "base";
      storage.update(item);

      storage.getItem(0).should.deep.equal({
        "_id": 0,
        "email": "john@me.com",
        "instrument": "base",
        "name": "John Doe"
      });
    });

    it('should be retrieved using the id as string', function() {
      const item = storage.create(profileWithEmailNameAndInstrument);
      item["instrument"] = "base";
      storage.update(item);

      storage.getItem("0").should.deep.equal({
        "_id": 0,
        "email": "john@me.com",
        "instrument": "base",
        "name": "John Doe"
      });
    });
  });

  describe("querying records", function() {
    it("should return an empty list when there is nothing stored", function() {
      storage.query().should.deep.equal([]);
    });

    it("should return a stored item", function() {
      const result = storage.create(profileWithEmailNameAndInstrument)
      storage.query().should.deep.equal([ result ]);
    });

    it("should return the item when it matches the query", function() {
      const result = storage.create(profileWithEmailNameAndInstrument)
      storage.query({ name: profileWithEmailNameAndInstrument.name }).should.deep.equal([ result ]);
    });

    it("should return an empty list when it does not match the query", function() {
      storage.create(profileWithEmailNameAndInstrument)
      storage.query({ name: "something" }).should.deep.equal([ ]);
    });
  });
});