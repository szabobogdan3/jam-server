module.exports = {
  jamPending: {
    status: "pending",
    location: "inside",
    when: "2021-06-24T19:00:00",
    song: 0,
    participants: [ ]
  },

  jamLive: {
    status: "live",
    location: "somewhere",
    when: "2020-06-24T19:00:00",
    song: 0,
    participants: [ ]
  }
}