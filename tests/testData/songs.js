module.exports = {
  songWithAllFields: {
    title: "A Man Is a Man",
    artist: "the Who",
    writer: "Pete Townshend",
    year: "1969",
    instruments: [ "guitar", "base", "vocal" ]
  }
}