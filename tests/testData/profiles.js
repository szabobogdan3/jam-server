module.exports = {
  profileWithEmailNameAndInstrument: {
    name: "John Doe",
    email: "john@me.com",
    instrument: "guitar"
  },

  profileWithEmailAndName: {
    name: "John Doe",
    email: "john@me.com"
  }
}